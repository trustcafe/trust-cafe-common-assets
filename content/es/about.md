## Acerca de

**Trust Café** se creó con el objetivo de combatir la desinformación en el ámbito de las redes sociales. Somos una plataforma dirigida y moderada por la comunidad, dedicada a crear un espacio donde las discusiones puedan prosperar y la desinformación se elimine rápidamente. Para más información sobre la filosofía de nuestra plataforma, [**consulta nuestras Preguntas Frecuentes**](faqs).

### Jimmy Wales - Fundador
Jimmy Wales es el fundador de Wikipedia y cofundador de Fandom. Trust Café es su proyecto piloto para intentar construir una plataforma de redes sociales más saludable (para ti y para el mundo) centrada en amplificar voces de calidad en línea.

### Orit - Recaudadora de Fondos
Orit es fundadora de Glass Voices, cofundadora de WikiTribune, ex-CEO de la Jimmy Wales Foundation y activista de derechos humanos.

### Fin - Jefe de Comunidad/Desarrollador/Cofundador
Fin ha estado involucrado en comunidades colaborativas en línea desde 2007, comenzando como administrador de Wikipedia. Ahora dedica su tiempo libre a investigar comunidades extremistas y educar a las personas sobre la desinformación en línea.

### Jezza - Administrador de Comunidad/Desarrollador Junior
Jezza se unió al proyecto Trust Café de WikiTribune en 2019 debido a la frustración con las redes sociales financiadas por anuncios y que fomentan la discusión. Ha trabajado en varios campos, incluyendo jardines y pastizales. Jez vive con su gato, Cupcake, y disfruta de un estilo de vida semi-nómada en un autobús escolar convertido.

### Simon - Desarrollador Principal/Cofundador
Simon ha sido desarrollador durante unas dos décadas y ha visto muchos cambios en la industria. Comenzó a programar cuando era joven creando juegos de aventuras basados en texto. Motivado por el auge de la desinformación impulsada por eventos como el Brexit y Trump, decidió usar sus habilidades para combatir la desinformación.
