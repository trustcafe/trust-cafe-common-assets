# Preguntas Frecuentes (FAQ)

## ¿Es WT.Social una plataforma de libertad de expresión?

No. Enfáticamente no. La misión de WT.Social es construir una plataforma de redes sociales no tóxica, lo que no puede suceder en un entorno donde todo vale. Adoptamos una postura proactiva contra el discurso de odio, el acoso y la desinformación.

## ¿Es WT.Social una plataforma políticamente de izquierda/derecha?

Nuestras directrices son políticamente agnósticas. Las inclinaciones políticas de los usuarios no se tienen en cuenta al tomar decisiones de moderación. Lo que queremos fomentar es el <i>discurso razonado</i> entre personas que pueden tener muchos desacuerdos. Cualquier miembro de la comunidad que pueda abstenerse de hablar de odio, acoso y desinformación es bienvenido. Habiendo dicho eso, debería ser evidente (pero desafortunadamente no es así) que los nazis y otras personas con inclinaciones genocidas no son bienvenidos aquí.

## ¿Qué es la moderación entre pares?

Exactamente como suena. Aunque los miembros del personal pueden intervenir y lo harán para eliminar a los usuarios que violen nuestras pautas y para proteger a los usuarios de los peores excesos de Internet, la moderación diaria de la plataforma puede y debe ser manejada por nuestros usuarios.<br/><br/>¿Por qué? Porque la moderación de arriba hacia abajo (o peor aún, la moderación impulsada por IA) no funciona. La moderación de Facebook ha creado un <a href='https://www.irishtimes.com/business/technology/life-as-a-facebook-moderator-people-are-awful-this-is-what-my-job-has-taught-me-1.4184711'>clase de empleados con PTSD</a> con la incapacidad de actuar cuando ven contenido evidentemente dañino.<br/><br/>La moderación algorítmica a menudo crea políticas que refuerzan los sesgos sociales existentes y pueden <a href='https://www.washingtonpost.com/technology/2021/11/21/facebook-algorithm-biased-race/'>promover el racismo</a>. Las empresas de redes sociales que dependen únicamente de los ingresos generados por la publicidad <a href='https://www.forbes.com/sites/petersuciu/2021/08/02/spotting-misinformation-on-social-media-is-increasingly-challenging'>permiten la propagación de desinformación</a> por el bien de la interacción y los clics. Y no es escalable. La IA <a href='https://www.digitalinformationworld.com/2021/02/ai-content-moderation-systems-are.html'>actualmente no puede mantenerse al día</a> con la naturaleza en constante cambio del comportamiento humano, e incluso los gigantes tecnológicos no pueden permitirse contratar suficientes moderadores humanos para vigilar sus plataformas.<br/><br/>WT.Social permite a los usuarios editar publicaciones y ocultar comentarios. Pueden combatir directamente el spam, solicitar e iniciar verificaciones colaborativas de hechos y, de lo contrario, moldear la plataforma para que sea lo que ellos quieren que sea. Es una idea radical, pero creemos firmemente que es el futuro.

## ¿Quién financia WT.Social?

Usuarios como tú. No hemos tomado dinero de VC, ni permitimos anunciantes en nuestra plataforma. Confiamos en la generosidad de nuestra comunidad para mantener las luces encendidas. Si desea ayudar, por favor considere hacer una contribución <a href='/membership'>aquí</a>.

## ¿Puedo promocionar mi blog/podcast/negocio aquí?

¡Sí, de hecho! Siempre y cuando siga nuestras pautas de autopromoción tal como se establece <a href='/self-promotion-policy'>aquí</a>.

## ¿Por qué se llama WT.Social?

WT.Social comenzó como un intento de crear contenido de noticias dirigido por el usuario. Con ese fin, nos nombramos WikiTribune (como en el periódico, no el foro romano). Aunque había interés en la idea, había demasiadas barreras para hacer una plataforma floreciente. A la luz de esto, giramos para crear valor en el espacio de las redes sociales. WikiTribune se acortó a WT.

## ¿Estás abierto a estar en la Blockchain?

No en este momento, ya que no hemos visto ningún caso de uso persuasivo para blockchain para nuestras necesidades reales. ¡Sin embargo, estamos abiertos a recibir apoyo financiero en prácticamente cualquier moneda, incluidas las criptomonedas!
