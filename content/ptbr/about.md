## Sobre

**Trust Café** foi criado com o objetivo de combater a desinformação nas redes sociais. Somos uma plataforma liderada e moderada pela comunidade, dedicada a criar um espaço onde discussões possam prosperar e a desinformação seja removida rapidamente. Para mais informações sobre a filosofia da nossa plataforma, [**consulte nosso FAQ**](faqs).

### Jimmy Wales - Fundador
Jimmy Wales é o fundador da Wikipedia e cofundador da Fandom. O Trust Café é seu projeto piloto para tentar construir uma plataforma de redes sociais mais saudável (para você e para o mundo) focada em amplificar vozes de qualidade online.

### Orit - Captadora de Recursos
Orit é fundadora do Glass Voices, cofundadora do WikiTribune, ex-CEO da Jimmy Wales Foundation e ativista dos direitos humanos.

### Fin - Chefe de Comunidade/Desenvolvedor/Cofundador
Fin está envolvido com comunidades colaborativas online desde 2007, começando como administrador da Wikipedia. Agora dedica seu tempo livre a pesquisar comunidades extremistas e educar as pessoas sobre desinformação online.

### Jezza - Gerente de Comunidade/Desenvolvedor Júnior
Jezza se juntou ao projeto Trust Café do WikiTribune em 2019 devido à frustração com redes sociais financiadas por anúncios e que fomentam a discussão. Trabalhou em vários campos, incluindo jardins e pastagens. Jez vive com seu gato, Cupcake, e desfruta de um estilo de vida semi-nômade em um ônibus escolar convertido.

### Simon - Desenvolvedor Principal/Cofundador
Simon é desenvolvedor há cerca de duas décadas e viu muitas mudanças na indústria. Começou a programar quando era jovem criando jogos de aventura baseados em texto. Motivado pelo aumento da desinformação impulsionada por eventos como o Brexit e Trump, decidiu usar suas habilidades para combater a desinformação.
