# Häufig Gestellte Fragen (FAQ)

## Warum heißt es Trust Café?

Trust Café begann als Versuch, benutzergetriebene Nachrichteninhalte zu erstellen. Zu diesem Zweck nannten wir uns damals [WikiTribune](https://dictionary.cambridge.org/dictionary/english/tribune) (wie in Zeitung, nicht das römische Forum). Obwohl es Interesse an der Idee gab, gab es zu viele Barrieren, um eine florierende Plattform zu schaffen. In Anbetracht dessen haben wir uns entschieden, im Bereich der sozialen Medien Wert zu schaffen. WikiTribune wurde zu WT verkürzt und die Website WT.Social wurde geboren.

Im Laufe der Zeit verlagerte sich unser Fokus auf die Schaffung eines sozialen Netzwerks, bei dem Vertrauen und Gemeinschaft an erster Stelle stehen — ein Ort, an dem Ehrlichkeit und Authentizität über allem geschätzt werden. Wir hoffen, Inhalte basierend auf Vertrauenswürdigkeit und nicht auf Engagement oder Sucht zu fördern. Die warme und einladende Atmosphäre eines physischen Cafés ist die Inspiration hinter Trust Café, einem Ort, an dem Sie sich in einer ehrlichen und freundlichen Umgebung mit Gleichgesinnten verbinden können.

## Was ist Vertrauen?

Das Vertrauensniveau eines Benutzers beeinflusst, wie und wo seine Inhalte anderen sichtbar sind. Es wirkt sich auch darauf aus, wie viel Gewicht ihre Stimmen haben und kann zusätzliche Berechtigungen auf höheren Ebenen gewähren. Trust Café verwendet ein vertrauensbasiertes System, bei dem die Vertrauensstufen der Benutzer die Sichtbarkeit ihrer Inhalte für andere, das Gewicht ihrer Stimmen und zusätzliche Berechtigungen bestimmen.

Das System hat mehrere Vertrauensstufen, beginnend mit Neuling/Gast und fortschreitend zu Bürger, Bronze, Silber, Gold und darüber hinaus mit zunehmendem Stimmgewicht. Die Kriterien für die Stufen reichen von der Vermeidung böswilligen Verhaltens bis hin zu einem Power-User oder einem außergewöhnlich vertrauenswürdigen Mitglied der Gemeinschaft. Die Stufen schalten bestimmte Berechtigungen und Fähigkeiten frei. Zum Beispiel schaltet die Gold-Stufe Verwaltungstools für die Community frei, und die Double Diamond-Stufe schaltet die Möglichkeit frei, Konten sofort auf die Silber-Stufe zu befördern. [Weitere Informationen zu unserem Vertrauensmaßstab finden Sie hier.](/trust)

## Was sind Branches?

Inhalte hier sind nach Themen in Branches unterteilt, die wie Subreddits oder Wikipedia-Projekte sind. Trust Café hat Branches für alles von Gartenarbeit bis Weltpolitik. Wenn Sie die Silber-Stufe erreichen, können Sie Branches erstellen.

[Sie können alle Trust Café Branches hier durchsuchen.](/branches/alphabetical) Wenn Sie noch nicht in der Lage sind, Branches zu erstellen, und keinen Branch für Ihr Lieblingsthema finden können, [besuchen Sie den "Request A Branch"-Branch.](/wt/request-a-branch)

## Muss ich meinen echten Namen verwenden?

Obwohl wir nach einem Vor- und Nachnamen fragen, um eine Kultur der Verantwortung zu fördern, sind Sie nicht verpflichtet, Ihren rechtlichen Namen auf unserer Website zu verwenden. Datenschutz ist sehr wichtig, und wir erkennen an, dass nicht jeder seine Beiträge mit seinem Namen verknüpfen möchte. [Bitte kontaktieren Sie uns, wenn Sie Bedenken bezüglich Ihres Anzeigenamens haben.](/contact)

## Werden mein Benutzername und Passwort von WT.Social bei Trust Café funktionieren?

Irgendwann ja, jedoch müssen Sie im Moment ein neues Konto bei Trust Café erstellen. Wenn Sie dieselbe E-Mail-Adresse verwenden, die Sie für Ihr WT.Social-Konto verwendet haben, um sich bei Trust Café anzumelden, werden die beiden Konten bei der Datenmigration miteinander verknüpft. Wenn Sie die E-Mail-Adresse ändern möchten, die mit Ihrem Konto verknüpft ist, [kontaktieren Sie uns bitte.](/contact)

## Ist Trust Café eine "Freie Rede Plattform"?

Nein. Eindeutig nicht. Die Mission von Trust Café ist es, eine nicht-toxische Social-Media-Plattform bereitzustellen, was in einem Umfeld, in dem alles erlaubt ist, nicht möglich ist. Wir gehen proaktiv gegen Hassrede, Belästigung und Fehlinformationen vor.

## Ist Trust Café eine links/rechts gerichtete Plattform?

Nein. Unsere Richtlinien sind politisch neutral. Die politischen Neigungen der Benutzer werden bei Moderationsentscheidungen nicht berücksichtigt. Was wir fördern möchten, ist **vernünftige Diskussionen** zwischen Menschen, die viele Meinungsverschiedenheiten haben. Jedes Community-Mitglied, das sich von Hassrede, Belästigung und Fehlinformationen fernhalten kann, ist willkommen. Allerdings sollte (leider ist es das nicht) selbstverständlich sein, dass Nazis und andere genocidale Personen hier nicht willkommen sind.

## Was ist Peer-to-Peer Moderation?

Genau das, was es klingt. Obwohl Mitarbeiter eingreifen können und werden, um Benutzer zu entfernen, die gegen unsere Richtlinien verstoßen, und um Benutzer vor den schlimmsten Auswüchsen des Internets zu schützen, kann und sollte die alltägliche Moderation der Plattform von unseren Benutzern übernommen werden.

Warum? Weil top-down Moderation (oder schlimmer, KI-gesteuerte Moderation) nicht funktioniert. Facebook-Moderatormitarbeiter haben eine gesamte [Klasse von Mitarbeitern mit PTSD](https://www.irishtimes.com/business/technology/life-as-a-facebook-moderator-people-are-awful-this-is-what-my-job-has-taught-me-1.4184711) geschaffen, die nicht in der Lage sind, zu handeln, wenn sie offensichtlich schädliche Inhalte sehen.

Algorithmische Moderation schafft oft Richtlinien, die bestehende soziale Vorurteile verstärken und [Rassismus fördern](https://www.washingtonpost.com/technology/2021/11/21/facebook-algorithm-biased-race/). Social-Media-Unternehmen, die ausschließlich auf Werbung angewiesen sind, [lassen Fehlinformationen verbreiten](https://www.forbes.com/sites/petersuciu/2021/08/02/spotting-misinformation-on-social-media-is-increasingly-challenging) um Engagement und Klicks willen. Und es ist nicht skalierbar. KI [kann derzeit nicht mithalten](https://www.digitalinformationworld.com/2021/02/ai-content-moderation-systems-are.html) mit der sich ständig ändernden Natur des menschlichen Verhaltens, und selbst Technologiegiganten können es sich nicht leisten, genug menschliche Moderatoren einzustellen, um ihre Plattformen zu überwachen.

Trust Café ermöglicht es Benutzern, Beiträge zu bearbeiten und Kommentare zu verstecken. Sie können direkt Spam bekämpfen, faktische Überprüfungen anfordern und starten und die Plattform auf andere Weise so gestalten, wie sie es möchten. Es ist eine radikale Idee, aber wir glauben fest, dass es die Zukunft ist.

## Wer finanziert Trust Café?

Benutzer wie Sie. Wir haben kein Risikokapital aufgenommen und erlauben keine Werbung auf unserer Plattform. Wir sind auf die Großzügigkeit unserer Community angewiesen, um die Lichter am Laufen zu halten. Wenn Sie helfen möchten, erwägen Sie bitte, eine Spende [hier](/membership) zu leisten.

## Kann ich meinen Blog/Podcast/Mein Unternehmen hier bewerben?

Ja, das können Sie! Solange Sie sich an unsere [Selbstwerbungsrichtlinien](/self-promotion-policy) halten.

## Haben Sie eine API (Application Programming Interface)?

Ja, das tun wir! [Lesen Sie die Dokumentation hier.](https://trustcafe.readme.io/reference/introduction)

## Wird die Website die Föderation über ActivityPub unterstützen?

Obwohl dies noch nicht umgesetzt ist, sind wir offen für Föderation und begrüßen Merge-Requests von engagierten Freiwilligen, die Trust Café mit dem Fediverse über das ActivityPub-Protokoll verbinden möchten.

## Sind Sie offen für die Blockchain?

Derzeit nicht, da wir keinen überzeugenden Anwendungsfall für die Blockchain für unsere tatsächlichen Bedürfnisse gesehen haben. Wir sind jedoch offen für finanzielle Unterstützung in nahezu jeder Währung, einschließlich Kryptowährung!
