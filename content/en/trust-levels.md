## Trust Café Ranking System

**Note: This is a draft document of ideas - we are seeking feedback and clarifying questions!**
At Trust Café, trusted content is king. A user’s trust level influences how and where their content is visible to others. It also affects how much weight their votes carry and can confer some additional permissions at higher levels. When a user is promoted (say from Bronze to Silver), their votes will retroactively count for more, thus automatically moving up the people beneath them.

Below is a quick (but not fully comprehensive) overview of each level of trust.

### Newbie/Guest

- **Description**: This is for new users and helps ensure that spammers or other bad actors don’t enter the wider trust ecosystem. After a brief period of time or sufficient constructive contributions, a user will be promoted from this rank.
- **Unlocked**: None
- **Vote Weight**: 0

### Citizen

- **Description**: Users have shown they are not malicious actors and have now moved one level up to where they can enter the trust ecosystem.
- **Unlocked**: Other users can vote to move you up the trust system
- **Vote Weight**: 0

### Bronze

- **Description**: Users have been voted trustworthy by enough members of the community that they are now able to give out trust points. This level is also the default for people who have joined the Trust Café membership club(?).
- **Unlocked**: The ability to grant trust points to another user
- **Vote Weight**: 1

### Silver

- **Description**: Users have been active or prominent enough on the website to gain a good level of trust. This is where we would expect the average active community member to be.
- **Unlocked**: The ability to create branches
- **Vote Weight**: 2

### Gold

- **Description**: To reach this level, a user will have had to accrue 180 trust points. Users at gold level will have some markings on their account to delineate them from lower trust levels.
- **Unlocked**: Community administration tools
- **Vote Weight**: 4

### Platinum

- **Description**: A user will have accrued 1620 trust points, which we do not expect anyone to reach for quite some time. This is exclusively for power users or exceptionally trustworthy members of the community.
- **Unlocked**: Zoom party (yes, really)
- **Vote Weight**: 8

### Diamond

- **Description**: See platinum. Just…moreso
- **Unlocked**: The ability to instantly promote accounts to Bronze level
- **Vote Weight**: 16

### Double Diamond

- **Description**: Like platinum just (literally) exponentially moreso.
- **Unlocked**: The ability to instantly promote accounts to Silver level
- **Vote Weight**: 32
