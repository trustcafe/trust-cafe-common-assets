# Data Transparency and Privacy Policy

**GDPR Audit**
*Undertaken May 18, 2018 by Jimmy Wales*

For all of the below, the “lawful basis” for processing the information is that it is necessary to our work as a social media platform and that we have a legitimate interest in processing the information. We do not rely upon consent as lawful basis, although we do sometimes ask for consent.

## Information collected as part of Signing up and using the site

- Email Address
- Password
- First name
- Last name

## If you choose to fill out your profile, we also ask for this information:

- Bio
- Photo

For profile information you have the ability to edit or delete it at any time.

## Information collected as part of Paying

As a matter of general policy the data collected as a part of paying to support WT.Social (hereafter referred to as Trust Café) is held by our third-party payment processors. We will download this data or parts of this data from time to time as needed to contact people for legitimate business purposes relating to the transactions or membership of the website.

**At PayPal**

- Name
- Email Address
- Home address (in some cases)

**At Stripe**

- Name
- Email Address
- Last 4 digits of credit card number
- Expiration date of credit card
- Credit card type (Visa / MC / etc.)

## Information that we share with others

We use Amazon Web Services including AWS S3 (file uploads and database backups), AWS RDS (databases), and AWS Elasticache (a Redis instance) as well as our primary web servers on Amazon EC2. We also use AWS SES for sending emails from the site.

Our Trust Café email service is with Google G-Suite. Obviously, any personal data that you email to us (such as a CV) will end up there and potentially saved by us indefinitely.

Similarly, we use Zendesk for our customer service emails, and any personal data you email to various official addresses will end up there and potentially saved by us indefinitely.

We frequently use Google Docs for internal and external document sharing

**Among the outside services that we use that we think have no personally identifiable information include**

- Gitlab (our code)
- Cloudflare (firewall and caching layer with outside world)

I have a keen interest in reducing our reliance on all outside tools so that we have better long-term control of our data and the privacy of our users. In some cases, there should be open-source and self-hosted alternatives that will be reasonable replacements for these industry-standard tools.

## Right to Erasure

Under GDPR, individuals have a non-absolute right to erasure under certain limited circumstances. As we do not rely upon consent as our lawful basis for holding data, a revocation of consent is not particularly relevant in our circumstances.

In particular, if you have been banned from our platform we may retain your email address or name to ensure the account cannot be recreated.

If you choose to leave the site and remove your account, then your email address will be deleted from the site’s live database, and all your profile information outlined above will be deleted as well. We do this because in this case, there would be no overriding legitimate interest nor public interest in our keeping the information in the site’s live database.

## Children’s Personal Data

It is not our intention to process data relating to children, and if we are informed that we are, we will take the appropriate action to stop doing so and delete the data.

## Objections and complaints

Any questions or objections about how we are processing data, including requests for erasure, should be directed to info@trustcafe.io and we will endeavor to respond within the 30 days required by GDPR.
