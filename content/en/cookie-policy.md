## Cookie Policy

**The cookie settings on this website are set to “allow cookies” to give you the best browsing experience possible. If you continue to use this website without changing your cookie settings or you click “Accept” below, you are consenting to this. We are committed to preserving the privacy of all visitors to this website. Please read the following privacy policy statement to understand how we collect and use personal data on this website:**

**1. Collection of Personal Information**
We collect personal information which you voluntarily submit to us, for example, when you complete our Contact form.

**2. Use of Personal Information**
We will only use the information which we collect about you lawfully and in a fair manner, in accordance with the Data Protection Act 1998.

**3. Cookies**
We may also collect information about your usage of the website using cookies. If you do not want to receive cookies, you may reject them by using your browser settings.

**4. Purpose of Collected Data**
Any information we collect about you will be used solely for internal marketing purposes and to improve this website. We do not sell or otherwise disclose your personal information to third parties, unless required to do so by law or court order, or to enable third parties to provide certain services to us. In such events, we will at all times control and be responsible for any such use of your personal information. We will keep your data securely to prevent unauthorized access by third parties.

**5. Communication**
We may use your personal information to provide you with newsletters and other information about our firm and services. Such newsletters or other information will contain clear instructions about how to unsubscribe should you decide that you no longer want to receive them.

**6. Third-Party Links**
In the event that this website contains links to other websites, we are not responsible for the privacy policies or other practices of the operators of any such websites.

**7. Liability**
Please note that Trust Café omits all liability for the content of any third-party website that is linked to its website.

**8. Mergers**
Should WIKITRIBUNE LTD (hereafter referred to as Trust Café) merge with another business entity, your information may be disclosed to and used by our new business partners or owners.

**9. Changes to the Policy**
We may change this statement at any time by posting revisions on this website.

**10. Legal Disclaimer**
Any content or commentary published on this website or on any of our social media platforms is for information purposes only and does not constitute specific legal advice. The information contained in this website is a non-exhaustive overview of a number of legal issues, and specific legal advice should be sought should you require personal or business advice on a client-professional services relationship basis.
