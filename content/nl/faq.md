# Veelgestelde Vragen (FAQ)

## Waarom heet het Trust Café?

Trust Café begon als een poging om door gebruikers aangedreven nieuwsinhoud te creëren. Daartoe hebben we onszelf destijds [WikiTribune](https://dictionary.cambridge.org/dictionary/english/tribune) genoemd (als in krant, niet het Romeinse forum). Hoewel er interesse was in het idee, waren er te veel barrières om een bloeiend platform te creëren. In het licht hiervan hebben we onze focus verlegd naar het creëren van waarde in de sociale media-ruimte. WikiTribune werd ingekort tot WT, en de website WT.Social werd geboren.

Naarmate de tijd vorderde, verschoof onze focus naar het creëren van een sociaal netwerk waar vertrouwen en gemeenschap voorop staan — een plek waar eerlijkheid en authenticiteit boven alles worden gewaardeerd. We hopen inhoud te bevorderen op basis van betrouwbaarheid, in plaats van betrokkenheid of verslaving. De warme en uitnodigende sfeer van een fysiek café is de inspiratie achter Trust Café, een plek waar je kunt verbinden met gelijkgestemde mensen in een eerlijke en vriendelijke omgeving.

## Wat is Trust?

Het vertrouwensniveau van een gebruiker beïnvloedt hoe en waar hun inhoud zichtbaar is voor anderen. Het beïnvloedt ook hoeveel gewicht hun stemmen hebben en kan extra bevoegdheden verlenen op hogere niveaus. Trust Café gebruikt een op vertrouwen gebaseerd systeem waarbij het vertrouwensniveau van gebruikers bepaalt hoe zichtbaar hun inhoud is voor anderen, het gewicht van hun stemmen en extra bevoegdheden.

Het systeem heeft verschillende niveaus van vertrouwen, te beginnen met Newbie/Guest en voortschrijdend naar Citizen, Bronze, Silver, Gold, en verder met toenemende stemgewichten. De criteria voor de niveaus variëren van simpelweg geen kwaadwillende actor zijn, tot een power user of een uitzonderlijk betrouwbaar lid van de gemeenschap zijn. De niveaus ontgrendelen bepaalde bevoegdheden en mogelijkheden. Bijvoorbeeld, het Gold-niveau ontgrendelt Community-beheer tools, en het Double Diamond-niveau ontgrendelt de mogelijkheid om accounts onmiddellijk naar het Silver-niveau te promoveren. [Lees hier meer over onze Vertrouwensmeting.](/trust)

## Wat zijn Branches?

Inhoud hier is verdeeld per onderwerp in Takken, die vergelijkbaar zijn met subreddits of Wikipedia-projecten. Trust Café heeft takken voor alles van Tuinieren tot Wereldpolitiek. Wanneer je het Silver-rang bereikt, kun je takken creëren.

[Je kunt alle Trust Café-takken hier doorbladeren.](/branches/alphabetical) Als je nog niet in staat bent om takken te creëren en geen tak kunt vinden voor je favoriete onderwerp, [bezoek dan de "Vraag Een Tak Aan" tak.](/wt/request-a-branch)

## Moet ik mijn echte naam Gebruiken?

Hoewel we om een voor- en achternaam vragen om een cultuur van verantwoordelijkheid te bevorderen, ben je niet verplicht om je juridische naam op onze site te gebruiken. Privacy is erg belangrijk en we erkennen dat niet iedereen zijn berichten met zijn naam wil associëren. [Neem contact met ons op als je bezorgd bent over je weergavenaam.](/contact)

## Werken mijn gebruikersnaam En wachtwoord van WT.Social op Trust Café?

Uiteindelijk wel, maar op dit moment moet je een nieuw account aanmaken op Trust Café. Als je hetzelfde e-mailadres gebruikt dat je voor je WT.Social-account hebt gebruikt om je aan te melden bij Trust Café, worden de twee accounts gekoppeld wanneer de gegevens worden gemigreerd. Als je het e-mailadres dat aan je account is gekoppeld wilt wijzigen, [neem dan contact met ons op.](/contact)

## Is Trust Café een 'Vrije Spreekplatform'?

Nee. Enkels niet. De missie van Trust Café is om een niet-toxisch sociaal mediaplatform te bieden, wat niet mogelijk is in een omgeving waar alles is toegestaan. We nemen een proactieve houding aan tegen haatzaaien, intimidatie en desinformatie.

## Is Trust Café Een Linkse/Rechtse Platform?

Nee. Onze richtlijnen zijn politiek agnostisch. De politieke voorkeuren van gebruikers worden niet meegewogen bij het nemen van moderatiebeslissingen. Wat we willen aanmoedigen is **redelijk-discours** tussen mensen die veel meningsverschillen kunnen hebben. Elk lid van de gemeenschap dat zich kan onthouden van haatzaaien, intimidatie en desinformatie is welkom. Dat gezegd hebbende, zou het (maar helaas is het dat niet) vanzelfsprekend moeten zijn dat nazi’s en andere genocidale mensen hier niet welkom zijn.

## Wat Is Peer-to-Peer Moderatie?

Precies wat het klinkt. Hoewel medewerkers kunnen en zullen ingrijpen om gebruikers te verwijderen die onze richtlijnen overtreden en om gebruikers te beschermen tegen de ergste uitwassen van het internet, kan en moet de dagelijkse moderatie van het platform door onze gebruikers worden afgehandeld.

Waarom? Omdat top-down moderatie (of erger, AI-gestuurde moderatie) niet werkt. Facebook-moderatie heeft een hele [klasse werknemers met PTSD](https://www.irishtimes.com/business/technology/life-as-a-facebook-moderator-people-are-awful-this-is-what-my-job-has-taught-me-1.4184711) gecreëerd met het onvermogen om te handelen wanneer ze overduidelijk schadelijke inhoud zien.

Algorithmische moderatie creëert vaak beleid dat bestaande sociale vooroordelen versterkt en kan [racisme bevorderen](https://www.washingtonpost.com/technology/2021/11/21/facebook-algorithm-biased-race/). Sociale-mediabedrijven die uitsluitend afhankelijk zijn van advertentie-inkomsten [laten desinformatie verspreiden](https://www.forbes.com/sites/petersuciu/2021/08/02/spotting-misinformation-on-social-media-is-increasingly-challenging) ten behoeve van betrokkenheid en klikken. En het is niet schaalbaar. AI [kan momenteel niet bijhouden](https://www.digitalinformationworld.com/2021/02/ai-content-moderation-systems-are.html) met de steeds veranderende aard van menselijk gedrag, en zelfs techreuzen kunnen het zich niet veroorloven om genoeg menselijke moderatoren in te huren om hun platforms te controleren.

Trust Café staat gebruikers toe om berichten te bewerken en reacties te verbergen. Ze kunnen spam direct bestrijden, fact-checks aanvragen en starten, en de platform anders vormgeven naar wat ze willen dat het is. Het is een radicale idee, maar we geloven vast dat het de toekomst is.

## Wie Financieret Trust Café?

Gebruikers zoals jij. We hebben geen durfkapitaal aangenomen, noch staan we adverteerders toe op ons platform. We vertrouwen op de gulheid van onze gemeenschap om de boel draaiende te houden. Als je wilt helpen, overweeg dan een bijdrage [hier](/membership).

## Kan Ik Mijn Blog/Podcast/Bedrijf Hier Promoten?

Ja, zeker! Zolang je onze [Zelfpromotie Richtlijnen](/self-promotion-policy) volgt.

## Hebben Jullie Een API (Application Programming Interface)?

Ja, dat hebben we! [Lees hier de documentatie.](https://trustcafe.readme.io/reference/introduction)

## Ondersteunt de site federatie via ActivityPub?

Hoewel dit nog niet is geïmplementeerd, staan we open voor federatie en verwelkomen we merge-aanvragen van enthousiaste vrijwilligers die Trust Café willen verbinden met de Fediverse via het ActivityPub-protocol.

## Zijn Jullie Open Voor Blockchain?

Niet op dit moment, omdat we geen overtuigende gebruiksgevallen voor blockchain voor onze werkelijke behoeften hebben gezien. We staan echter open voor financiële steun in vrijwel elke valuta, inclusief cryptocurrency!
