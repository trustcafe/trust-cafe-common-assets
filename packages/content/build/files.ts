import fs from 'fs-extra';
import path from 'path';
import MarkdownIt from 'markdown-it';
import { PascalCase,kebabCase, snakeCase } from "@sil/case";
import { processTOC, ProcessedTOCNode } from "../src/utils/toc";


interface Content {
    filename: string;
    md: string;
    html: string;
    toc: ProcessedTOCNode | undefined;
    title: string;
    lang: string;
}

const DESTINATION_PATH = "src/_generated";
const SOURCE_PATH = "src/data";

function escapeQuotes(str: string): string {
    return str.replace(/"/g, '\\"').replace(/'/g, "\\'");
}

const processMarkdown = async (file: string): Promise<Partial<Content>> => {
    const md = new MarkdownIt();
    const markdownItAnchor = await import('markdown-it-anchor');
    const markdownItTocDoneRight = await import('markdown-it-toc-done-right');

    const markdownData: Partial<Content> = {
        md: file,
        title: '',
        html: '',
        toc: undefined
    };

    md.use(markdownItAnchor.default);
    md.use(markdownItTocDoneRight.default, {
        callback: (_: string, data: any) => {
            const escapedTitle = escapeQuotes(data.c[0].n);
            markdownData.title = escapedTitle;
            markdownData.toc = processTOC(data.c[0]);
        }
    });
    markdownData.html = md.render(file);

    return markdownData;
};

const generateTsFile = async (filePath: string, content: Content) => {
    const relativePath = path.relative(SOURCE_PATH, filePath);
    const outputFilePath = path.join(DESTINATION_PATH, relativePath.replace('.md', '.ts'));
    await fs.ensureDir(path.dirname(outputFilePath));

    const outputContent = `
        export const content = ${JSON.stringify(content, null, 4)};
    `;
    await fs.writeFile(outputFilePath, outputContent, 'utf-8');
    return content;
};

const processMarkdownFiles = async (directory: string) => {
    const tsFiles: Content[] = [];
    const filesByLang: { [lang: string]: Content[] } = {};

    const traverseDirectory = async (dir: string) => {
        const files = await fs.readdir(dir);

        for (const file of files) {
            const filePath = path.join(dir, file);
            const stat = await fs.stat(filePath);

            if (stat.isDirectory()) {
                await traverseDirectory(filePath);
            } else if (path.extname(file) === '.md') {
                const content = await fs.readFile(filePath, 'utf-8');
                const lang = path.basename(path.dirname(filePath));

                const { html, toc, title } = await processMarkdown(content);

                const contentObj: Content = {
                    filename: path.basename(filePath),
                    md: content,
                    html: html || '',
                    title: title || '',
                    toc,
                    lang
                };

                const tsFile = await generateTsFile(filePath, contentObj);
                tsFiles.push(tsFile);

                if (!filesByLang[lang]) {
                    filesByLang[lang] = [];
                }
                filesByLang[lang].push(tsFile);
            }
        }
    };

    await traverseDirectory(directory);
    return { tsFiles, filesByLang };
};

const generateIndexFile = async (_: string, tsFiles: Content[]) => {
    const files = tsFiles.map((file) => {
        return {
            title: file.title,
            filename: file.filename,
            lang: file.lang,
        };
    });

    const exports = files.map(({ filename, lang, title }) => {
        return `{ name: '${filename.replace('.md', '')}', filename: '${filename}', lang: '${lang}', title: '${title}' }`;
    });

    const indexContent = `
        export const pages = [
            ${exports.join(',')}
        ];
    `;

    const indexPath = path.join(DESTINATION_PATH, 'index.ts');
    await fs.ensureDir(path.dirname(indexPath));
    await fs.writeFile(indexPath, indexContent, 'utf-8');
};

const generateLanguageFiles = async (filesByLang: { [lang: string]: Content[] }) => {
    for (const lang in filesByLang) {
        const files = filesByLang[lang];
        const imports = files.map((file) => {
            const importPath = `./${file.filename.replace('.md', '')}`;
            const importName = PascalCase(path.basename(file.filename, '.md'));
            return `import * as ${importName} from '${importPath}';`;
        });

        const exports = files.map((file) => {
            const exportName = PascalCase(path.basename(file.filename, '.md'));
            return `${exportName}`;
        });

        const langContent = `
            ${imports.join('\n')}

            export const ${snakeCase(lang)} = {
                ${
                    files.map((file) => {
                        const baseName = kebabCase(path.basename(file.filename, '.md'));
                        const exportName = PascalCase(baseName);
                        return `"${baseName}": ${exportName}`;
                    })
                }
            }
            export default ${snakeCase(lang)};
        `;

        const langDirPath = path.join(DESTINATION_PATH, lang);
        const langFilePath = path.join(langDirPath, 'index.ts');
        await fs.ensureDir(langDirPath);
        await fs.writeFile(langFilePath, langContent, 'utf-8');
    }
};

// Replace with the path to your markdown files
const markdownDirectory = path.resolve(process.cwd(), SOURCE_PATH);
processMarkdownFiles(markdownDirectory).then(({ tsFiles, filesByLang }) => {
    generateIndexFile(markdownDirectory, tsFiles).then(() => {
        generateLanguageFiles(filesByLang).then(() => {
            console.log('All files processed and index.ts and language files generated successfully.');
        });
    });
});