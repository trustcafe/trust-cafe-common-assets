import { processTOC, TOCNode, ProcessedTOCNode } from './toc';

describe('processTOC', () => {
  it('should process a single node correctly', () => {
    const toc: TOCNode = { l: 1, n: 'Title 1', c: [] };
    const expected: ProcessedTOCNode = { level: 1, title: 'Title 1', children: [] };

    expect(processTOC(toc)).toEqual(expected);
  });

  it('should process nested nodes correctly', () => {
    const toc: TOCNode = {
      l: 1,
      n: 'Title 1',
      c: [
        { l: 2, n: 'Title 1.1', c: [] },
        { l: 2, n: 'Title 1.2', c: [{ l: 3, n: 'Title 1.2.1', c: [] }] }
      ]
    };
    const expected: ProcessedTOCNode = {
      level: 1,
      title: 'Title 1',
      children: [
        { level: 2, title: 'Title 1.1', children: [] },
        { level: 2, title: 'Title 1.2', children: [{ level: 3, title: 'Title 1.2.1', children: [] }] }
      ]
    };

    expect(processTOC(toc)).toEqual(expected);
  });

  it('should handle empty children array', () => {
    const toc: TOCNode = { l: 1, n: 'Title 1', c: [] };
    const expected: ProcessedTOCNode = { level: 1, title: 'Title 1', children: [] };

    expect(processTOC(toc)).toEqual(expected);
  });

  it('should handle multiple nested levels', () => {
    const toc: TOCNode = {
      l: 1,
      n: 'Title 1',
      c: [
        {
          l: 2,
          n: 'Title 1.1',
          c: [
            { l: 3, n: 'Title 1.1.1', c: [] },
            { l: 3, n: 'Title 1.1.2', c: [] }
          ]
        }
      ]
    };
    const expected: ProcessedTOCNode = {
      level: 1,
      title: 'Title 1',
      children: [
        {
          level: 2,
          title: 'Title 1.1',
          children: [
            { level: 3, title: 'Title 1.1.1', children: [] },
            { level: 3, title: 'Title 1.1.2', children: [] }
          ]
        }
      ]
    };

    expect(processTOC(toc)).toEqual(expected);
  });
});