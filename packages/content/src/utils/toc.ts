export interface TOCNode {
    l: number;
    n: string;
    c: TOCNode[];
}

export interface ProcessedTOCNode {
    level: number;
    title: string;
    children: ProcessedTOCNode[];
}
export function processTOC(toc: TOCNode): ProcessedTOCNode {
    const processNode = (node: TOCNode): ProcessedTOCNode => {
        return {
            level: node.l,
            title: node.n,
            children: node.c.map(child => processNode(child))
        };
    };

    return processNode(toc);
}