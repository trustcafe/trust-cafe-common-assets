import { getContentFile, getContent } from './content';
import { Languages, ContentResult } from './types';

jest.mock('./_generated', () => ({
    pages: [
        { lang: 'en-gb', name: 'home', filename: 'home.md' },
        { lang: 'es-es', name: 'home', filename: 'home.md' },
        { lang: 'en-gb', name: 'about', filename: 'about.md' }
    ]
}));

jest.mock('./_generated/en-gb/index', () => ({
    home: {
        md: 'English Home Content',
        html: '<p>English Home Content</p>',
        title: 'Home',
        toc: []
    },
    about: {
        md: 'English About Content',
        html: '<p>English About Content</p>',
        title: 'About',
        toc: []
    }
}), { virtual: true });

jest.mock('./_generated/es-es/index', () => ({

    home: {
        md: 'Spanish Home Content',
        html: '<p>Spanish Home Content</p>',
        title: 'Inicio',
        toc: []
    }

}), { virtual: true });

describe('getContentFile', () => {
    it('should fetch content successfully', async () => {
        const result: ContentResult = await getContentFile('en-gb', 'home');

        expect(result.data?.md).toBe('English Home Content');
        expect(result.error).toHaveLength(0);
    });

    it('should return error if content not found', async () => {
        const result: ContentResult = await getContentFile('en-gb', 'nonexistent');
        expect(result.data?.md).toBe('');
        expect(result.error).toContain('Content not found');
    });
});

describe('getContent', () => {
    it('should fetch content successfully', async () => {
        const result: ContentResult = await getContent('es-es', 'home');

        expect(result.data?.md).toBe('Spanish Home Content');
        expect(result.error).toHaveLength(0);
    });

    it('should fallback to default language if content not found', async () => {
        const result: ContentResult = await getContent('es-es', 'about');
        expect(result.data?.md).toBe('English About Content');
        expect(result.error).toHaveLength(0);
    });

    it('should return error if content not found in both languages', async () => {
        const result: ContentResult = await getContent('es-es', 'nonexistent');
        expect(result.data?.md).toBe('');
        expect(result.error).toContain("Content not found");
    });
});