# Self Promotion Policy

### We're here to build a community. Please do not make an account to promote your products.
If your account exists simply to promote shirts or supplements that are *one trick doctors don't want you to know!* we do not want you here. Your account will be banned.

### If you want to make a branch for your fandom/you and your fans - we'd love to have you
Branches themed around podcasts/authors/tv shows etc. are extremely welcome. We love fandoms, and we hope they will make a home on Trust Café.

### If you want to share your content in relevant branches - please use common sense
If you want to share your blog post about AI in the [Technology branch](en/wt/technology) that's cool. Hopefully people will find it interesting. If you then go on to share it in 5 more branches or otherwise keep posting (especially without engaging with the wider community) your account will be banned.

### Are affiliate links allowed?
Affiliate links are not currently allowed on Trust Café.

*As ever, this is a working policy and not carved in stone. Please feel free to [open a discussion](en/wt/trustcafe-feedback) about how you think this could be improved.
Trust Café has a less prescriptive policy set than many other social media platforms. This is not because we are less committed to providing a safe and enjoyable environment for our users, but because our moderation is community-driven, and we trust their ability to discuss and adjudicate edge-cases. With that said...*

If you see spam, please downvote the user and give them a low trust rating. You can also alert an admin by adding a comment with the hashtag #spam.

**TL;DR Community building and interaction is great. Product promotion is not. Please [email us](mailto:info@wikitribune.com) for more details.**
