## Dear Supporters

I need your help.

All the problems in the world that I've set out to try to solve with WikiTribune Social are as bad as ever. We're all seeing the possibility that Elon Musk will buy Twitter and turn it into a radical free speech troll zone. He's notorious himself for bad behavior on Twitter—he definitely won't make Twitter less toxic.

He's promised to allow Donald Trump back on the platform in the name of 'free speech', but let's be clear about what he really wants: Trump kept Twitter on the front page of every newspaper in the world for half a decade, and it's very profitable for them to bring him back, no matter the cost.

WikiTribune has a different model. A strange model, to be sure, but it's one that I believe in. We don't want our platform to grow by focusing on having as many pageviews as possible by maximizing addiction and outrage. We want to build something that people like you care enough about to support voluntarily, financially. It's a 'pay if you want' model. I've joked many times— it may not be a great business model to give things away and hope people will voluntarily support them—but this is how I've built my career so far!

There's no simple way to avoid saying it. I've chosen not to raise venture capital money because I want to maintain full creative control to build the vision that I have in mind. Over the past two years, I've been personally the major funder of the project—but it's straining my personal resources.

Over the past several months, we've gone back to the codebase and completely re-architected it from scratch, pulling from everything that we've learned so far. We've got a great small community and the new software will have new ways to support and reward people for making quality posts (instead of how all other social networks reward people for making viral addictive fluff). Finishing this redesign will have a huge impact. I'm excited about it!

But to finish, I need your support. I have to pay for the development work to finish the new version of the site, and I need to do some PR and marketing to get the word out. I need to really start pushing this forward as a relaunch. If each of you can afford to give even a little, I think we have a real shot at making something revolutionary and new. We won't know unless we keep trying.

This is our chance to break the back of toxic social media by offering a better alternative. With Musk's impending Twitter purchase, and the US midterms approaching, time is short to create a space for healthier discourse.

## We are severely short on funds

_Jimmy Wales_
