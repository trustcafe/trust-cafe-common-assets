# Roadmap of Features

### Trust Rating
The first step to Jimmy's new vision is allowing users to rate each other.

### Mass archiving of a user's stuff
Typically used as part of blocking/account deletion.

### Let everyone see who trusts someone and how much
So we can all discuss how it's being used.

### Change trust rating bar so it is -20% to +100%
Allow users to express displeasure with someone.

### Let everyone see who is trusted by a user
So we can all discuss how it's being used.

### Let everyone see who is highly ranked on the site
Simple table or tables to show the top users.

### Trust Mechanism: Cause and effect
Implement first extremely basic algorithm to sort discover tab by time AND quality.

### People suggestions in Onboarding
So interesting new users (based on trust) we identify can get big followings quickly.

### Nuke a post and all comments entirely
...

### See who people are following and who follows them
So we can look into it.

### Bell Comment notifications
Bell notify me when someone comments on my post or my comment or edits those.

### Declutter notification
Remove bell notification when someone I follow posts.

### Email notifications
Have email notifications.

### 24 and 72 hour new user emails
The standard sort of emails but tells them what they need to do for promotion.

### Branch Moderation
Deleting and merging branches are tools we will need.

### Moderation Center
A place for people to discuss and debate any issues with the content.

### Beginners Guide
To help people get stuck in we will need this helpful guide.

### Open source WTS2
This will be an exciting day but there are some things we need to do first.

### 'Follow me' links
A feature we have on WTS1 which will help spread the word.

### Ability to follow a post
One of the most requested features.

### Newsletter Preferences
Before we can transition to WTS2 this will need doing.

### Pinned posts
A staple feature from WTS1 that is still missing from WTS2.

### Re-posting
A staple feature from WTS1 that is still missing from WTS2.

### User to user blocking
A staple feature from WTS1 that is still missing from WTS2.

### Ability to disable collaborative editing
A staple feature from WTS1 that is still missing from WTS2.

### Launch MVP
At this point we can switch over to WTS2 permanently.

### Regional personalisations
ie- Showing times in your timezone and local format.

### Post/comment/recent changes translation
We have been wanting to view posts in various languages for a long time now.

### Searching
The search algorithm for WTS1 is awful and we want to make sure its replacement is actually usable.

### Video uploads
Posting videos to social media websites is commonplace and should be something we support.

### OAuth Authorization Code Flow with PKCE
With this we will be able to start work on a mobile app or allow others to have a go.
