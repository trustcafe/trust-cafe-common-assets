# Names and Pseudonyms on Trust Café

**TL;DR Trust Café requires *a* name, it does not have to be *your* name.**

As ever, this is a working policy and not carved in stone. Please feel free to [open a discussion](en/wt/trustcafe-feedback) about how you think this could be improved.

Trust Café has a less prescriptive policy set than many other social media platforms. This is not because we are less committed to providing a safe and enjoyable environment for our users, but because our moderation is community-driven, and we trust their ability to discuss and adjudicate edge-cases. With that said...

## Trust Café Requires *a* First and Last Name, it Does Not Have to be *Your* Name.

There is a proven benefit to users having an assigned identity on social networks. They help foster a kinder and more cooperative environment. That is at the core of our naming policy. We are not interested in your government name unless you choose to verify yourself.

## Please Choose a Name Within the Realms of Possibility

Example: "Z" is a perfectly valid first name, "MyCatCanHaveALittleSalami" is not.

## Use Common Sense

Jean-Luc Picard, Daffy Duck, Donald Trump - admittedly they are all pseudonyms with a first and last name, but completely against the spirit of the project. Depending on the name, you may be asked to change it, or banned.

## Company or "Official" Names

We currently don't have any way of being able to tell that an account is "officially" from a company and as such you may be asked to change any name purporting to be the official account of any famous people or companies.

We are hoping to have verification for both companies and individuals soon but for now, this is a grey area. If you have further questions about this please email us at info{@}trustcafe[dot]io.
