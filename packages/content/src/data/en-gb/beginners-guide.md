## A Beginner's Guide to Trust Café

**Welcome to Trust Café! Below is a comprehensive guide to using our website. If you’ve found a bug, please make a post in [the Bugs branch](https://wts2.wt.social/en/wt/wts2-bugs). If you would like to suggest an improvement, visit [the Features branch](https://wts2.wt.social/en/wt/wts2-features).**

### INTRODUCTION

**What makes Trust Café different from Facebook and Twitter?**
Although you can see some similarities in our design, the social aspect of Trust Café is a lot different than what you might be used to. The website is dedicated to sharing trustworthy content and creating substantive discussions. You won’t see a lot of minion memes or other low-quality content here.

We also operate on a peer-to-peer moderation system. That means you are empowered to make this a great place for all. If you see spam, misinformation, or hate speech, let people know by using the trust rating system.

Staff can and will step in if necessary, but the community (including community administrators) are the heart of the website.

**What makes a ‘quality post’?**
A good quality Trust Café post should have accurate and up-to-date information, provide sources for any claims made, and be written in a clear, concise, and easy-to-understand manner. Additionally, the post should be respectful of other viewpoints and should not contain any offensive language or content.

**Which users are trustworthy?**
The trust system is a great way for users to rate other users as trustworthy when they post good quality content. It helps create an environment of accountability and encourages people to be more mindful about the content that they are posting. Additionally, it can also help identify untrustworthy people who may be intentionally posting spam or inappropriate material.

**Editing A Post**
You can edit your own posts on Trust Café, like on any social media website. Unlike other platforms, users can edit each other’s posts to correct mistakes, reword headlines, and flag misinformation. In this way, Trust Café is like a wiki: everyone can help curate quality content.

**Hiding Comments**
If you see a comment that shouldn’t be there, such as hate speech or advertisements, you can hide the post from view for yourself and everyone else. This is one of the ways the community helps keep Trust Café tidy. If a comment is hidden by mistake, it can be unhidden just as easily.

When a user hides or unhides a comment, the change is logged in the Recent Changes list so that everyone can see. In addition to encouraging action on bad actors with hidden comments, this transparency measure also prevents clandestine abuse of the system.

### ABOUT

**About Your Feed**
You’ll notice that your home feed has two tabs: ‘Your Feed’ shows you every post from people or branches you are following, and ‘For You’ shows you all the latest posts. As the trust network develops, new accounts and potential spammers will be displayed below trusted community members.

**About Branches**
A ‘branch’ is a place to post content about a given topic, like subreddits or Wikipedia Projects. Trust Café has branches for everything from US Politics to Puzzles from Around The World. [You can browse all Trust Café branches here.](branches/alphabetical)

**About Recent Changes**
Trust Café believes in being as transparent as possible. If you click on [‘Recent Changes’](recentchanges), you can see every action taken on the website. This helps keep everyone accountable and is a great way to look into how moderation works on the platform.

**Who Runs This Place?**
On the day-to-day level? People like you! For information about our staff team, please see [the ‘About Us’ page](About).

### HOW TO

**Change Your Settings**
You can update your language, notification settings, and other account details in [the ‘My Account’ page](myaccount) at any time. If you would like to help translate the site into your language, let us know in [the official Trust Café Discord server](https://discord.gg/ftyB5yzwH5).

**Talk to Another User**
You can update your language, notification settings, and other account details in [the ‘My Account’ page](myaccount) at any time. If you would like to help translate the site into your language, let us know in [the official Trust Café Discord server](https://discord.gg/ftyB5yzwH5).

**Invite Your Friends, Family, and Fans**
UNDER CONSTRUCTION

**Share a Post**
This should explain both on-platform and off-platform sharing since both are important to growth. To share a Trust Café post to your followers on other social media sites, use the share button [add image below] to share directly to Facebook or Twitter, or copy the link and paste it into an email or other message.

**Keep Track of Conversations**
THIS ISN’T FINISHED - NEED TO ADD THE ABILITY TO SUBSCRIBE TO A POST. When you make a post, you’ll get a bell notification whenever someone edits it, or leaves a comment. You can also turn on email alerts for updates on [the ‘My Account’ page](myaccount).

**Use Hashtags**
THIS ISN’T STARTED AND WE NEED A COMMUNITY DISCUSSION ABOUT IT

### MORE DETAILS

**Collaborative Editing**
Posts to Trust Café are collaborative by default, which allows anyone to edit your post. It sounds scary, but it really isn’t! A transparent ‘revision history’ is on the right-hand side, which shows what each person wrote and when they wrote it.

Community members usually reserve editing for minor improvements to posts, or alerting other users when something is wrong. We strongly encourage you to follow these practices. If you wish to post a personal non-collaborative post, uncheck the ‘Make this post collaborative’ option when submitting your post.

**Branches About Trust Café**
SAVING UNTIL SUBWIKIS ARE IMPORTED - THEN WE SHOULD COLLABORATE ON THIS. This section will need different links for different languages since the branches are separate.

**Terms of Service**
UNDER CONSTRUCTION: Add links to Self Promotion Policy, NSFW Policy, Names and Pseudonyms Policy

**Visual Guide**
Visual learner? Check out the [Visual Guide to WTS here](Visual Guide).
