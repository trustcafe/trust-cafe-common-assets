## About

**Trust Café** was created with the goal of fighting misinformation in the social media sphere. We are a community-led and community-moderated platform dedicated to creating a space where discussions can thrive, and misinformation is quickly removed. For more on our platform's philosophy, [**please see our FAQs**](faqs).

### Jimmy Wales - Founder
Jimmy Wales is the founder of Wikipedia and co-founder of Fandom. Trust Café is his pilot project to try to build a healthier (for you and for the world) social networking platform focused on amplifying quality voices online.

### Orit - Fund Raiser
Orit is the founder of Glass Voices, Co-Founder of WikiTribune, former CEO of the Jimmy Wales Foundation, and a human rights activist.

### Fin - Head of Community/Developer/Co-Founder
Fin has been involved with online collaborative communities since 2007, starting as a Wikipedia administrator. They now spend their off-time researching extremist communities and educating people about online misinformation.

### Jezza - Community Manager/Junior Dev
Jezza joined the Trust Café project of WikiTribune in 2019 due to frustration with advertisement-funded and argument-fueling social media. They have worked in various fields, including gardens and cow pastures. Jez lives with their cat, Cupcake, and enjoys a semi-nomadic lifestyle in a converted school bus.

### Simon - Lead Dev/Co-Founder
Simon has been a developer for about two decades and has witnessed many changes in the industry. He started coding as a young enthusiast making text-based adventure games. Motivated by the rise of misinformation driven by events like Brexit and Trump, he decided to use his skills to combat misinformation.
