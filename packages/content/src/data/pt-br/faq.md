# Perguntas Frequentes (FAQ)

## O Trust Café é uma 'Plataforma de Discurso Livre'?

Não. A missão do WT.Social é fornecer uma plataforma de comunicação social não tóxica, o que não pode acontecer num ambiente em que nada acontece. Tomamos uma posição proativa contra o discurso do ódio, o assédio e a desinformação.

## O WT.Social é uma plataforma de esquerda/direita?

Não. Nossas diretrizes são politicamente neutras. As inclinações políticas dos usuários não são consideradas ao tomar decisões de moderação. O que queremos incentivar é <strong>discussão razoável</strong> entre pessoas que podem ter muitas discordâncias. Qualquer membro da comunidade que possa se abster de discurso de ódio, assédio e desinformação é bem-vindo. Dito isso, deveria (mas infelizmente não) ser óbvio que nazistas e outras pessoas com tendências genocidas não são bem-vindos aqui.

## O que é a Moderação de Ponto-a-ponto?

Exatamente o que parece. Embora os membros da equipe possam e irão intervir para remover usuários que violam nossas diretrizes e proteger usuários dos piores excessos da internet, a moderação diária da plataforma pode e deve ser realizada por nossos usuários.<br/><br/>Por quê? Porque a moderação de cima para baixo (ou pior, moderação conduzida por IA) não funciona. A moderação do Facebook criou uma <a href='https://www.irishtimes.com/business/technology/life-as-a-facebook-moderator-people-are-awful-this-is-what-my-job-has-taught-me-1.4184711'>classe de funcionários com PTSD</a> incapaz de agir quando vêem conteúdo claramente prejudicial.<br/><br/>A moderação algorítmica frequentemente cria políticas que reforçam preconceitos sociais existentes e podem <a href='https://www.washingtonpost.com/technology/2021/11/21/facebook-algorithm-biased-race/'>promover racismo</a>. Empresas de mídia social que dependem exclusivamente de receita publicitária <a href='https://www.forbes.com/sites/petersuciu/2021/08/02/spotting-misinformation-on-social-media-is-increasingly-challenging'>permitir a disseminação de desinformação</a> em nome do engajamento e cliques. E isso não é escalável. IA <a href='https://www.digitalinformationworld.com/2021/02/ai-content-moderation-systems-are.html'>não consegue acompanhar</a> a natureza sempre mutável do comportamento humano, e mesmo gigantes da tecnologia não conseguem contratar moderadores humanos suficientes para monitorar suas plataformas.<br/><br/>WT.Social permite que os usuários editem posts e ocultem comentários. Eles podem combater diretamente o spam, solicitar e iniciar verificações de fatos colaborativas e moldar a plataforma da maneira que desejam. É uma ideia radical, mas acreditamos firmemente que é o futuro.

## Como posso levar os meus seguidores a confiar no Trust Café?

Fornecemos um link ao se inscrever que permite convidar pessoas para seguir você no WT.Social. A URL é <code>https://wt.social/YOUR-USERNAME-HERE/followme</code>.

## Quem financia o Trust Café?

Usuários como você. Nós não recebemos dinheiro de VC (Venture Capital), nem permitimos anunciantes em nossa plataforma. Contamos com a generosidade da nossa comunidade para manter as luzes acesas. Se quiser ajudar, considere fazer uma contribuição <a href='https://contribute.wt.social/'>aqui</a>.

## Posso promover o meu Blog/Podcast/Negócio aqui?

Você com certeza pode! Desde que siga as nossas orientações de autopromoção, conforme estabelecido em <a href='https://wt.social/self-promotion'>aqui</a>.

## Por que é chamado de Trust Café?

O WT.Social começou como uma tentativa de criar conteúdo de notícias gerado por usuários. Para isso, nos chamamos de WikiTribune (como em <a href='https://dictionary.cambridge.org/dictionary/english/tribune'>jornal</a>, não o fórum romano). Embora houvesse interesse na ideia, havia barreiras demais para criar uma plataforma próspera. À luz disso, mudamos nosso foco para criar valor no espaço das mídias sociais. WikiTribune foi abreviado para WT.

## Vocês estão abertos para estar na blockchain?

Não no momento, pois não vimos um caso de uso convincente para a blockchain para nossas necessidades atuais. No entanto, estamos abertos a apoio financeiro em praticamente qualquer moeda, incluindo criptomoeda!
