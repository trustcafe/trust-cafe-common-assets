## Über uns

**Trust Café** wurde mit dem Ziel gegründet, Fehlinformationen in sozialen Medien zu bekämpfen. Wir sind eine von der Community geleitete und moderierte Plattform, die sich der Schaffung eines Raums widmet, in dem Diskussionen gedeihen können und Fehlinformationen schnell entfernt werden. Weitere Informationen zur Philosophie unserer Plattform finden Sie in [**unseren FAQs**](faqs).

### Jimmy Wales - Gründer
Jimmy Wales ist der Gründer von Wikipedia und Mitbegründer von Fandom. Trust Café ist sein Pilotprojekt, um eine gesündere (für dich und für die Welt) Plattform für soziale Netzwerke zu schaffen, die darauf abzielt, qualitativ hochwertige Stimmen online zu verstärken.

### Orit - Fundraising
Orit ist Gründerin von Glass Voices, Mitgründerin von WikiTribune, ehemalige CEO der Jimmy Wales Foundation und Menschenrechtsaktivistin.

### Fin - Leiter der Community/Entwickler/Mitbegründer
Fin ist seit 2007 in Online-Kollaborationsgemeinschaften aktiv, beginnend als Administrator bei Wikipedia. Heute widmet er seine Freizeit der Forschung zu extremistischen Gemeinschaften und der Aufklärung über Online-Fehlinformationen.

### Jezza - Community-Manager/Junior-Entwickler
Jezza kam 2019 zum Trust Café-Projekt von WikiTribune, weil er frustri
