## Over

**Trust Café** is opgericht met het doel om desinformatie op sociale media tegen te gaan. We zijn een community-geleide en community-gemodereerde platform dat zich inzet voor het creëren van een ruimte waar discussies kunnen gedijen en desinformatie snel wordt verwijderd. Voor meer informatie over de filosofie van ons platform, [**bekijk onze FAQ**](faqs).

### Jimmy Wales - Oprichter
Jimmy Wales is de oprichter van Wikipedia en mede-oprichter van Fandom. Trust Café is zijn proefproject om een gezonder sociaal netwerkplatform te bouwen dat gericht is op het versterken van kwaliteitsstemmen online.

### Orit - Fondsenwerver
Orit is oprichter van Glass Voices, mede-oprichter van WikiTribune, voormalig CEO van de Jimmy Wales Foundation en mensenrechtenactivist.

### Fin - Hoofd Community/Developer/Mede-oprichter
Fin is sinds 2007 betrokken bij online samenwerkingsgemeenschappen, beginnend als Wikipedia-beheerder. Ze besteden nu hun vrije tijd aan het onderzoeken van extremistische gemeenschappen en het informeren van mensen over online desinformatie.

### Jezza - Community Manager/Junior Dev
Jezza kwam in 2019 bij het Trust Café-project van WikiTribune vanwege frustratie met advertenties gefinancierde en argumenten opwekkende sociale media. Ze hebben in verschillende velden gewerkt, waaronder tuinen en koeienweiden. Jez woont met hun kat, Cupcake, en geniet van een semi-nomadische levensstijl in een omgebouwde schoolbus.

### Simon - Lead Dev/Mede-oprichter
Simon is al ongeveer twee decennia ontwikkelaar en heeft veel veranderingen in de industrie meegemaakt. Hij begon met coderen als jonge enthousiasteling met het maken van tekstgebaseerde avonturenspellen. Gemotiveerd door de opkomst van desinformatie door gebeurtenissen zoals Brexit en Trump, besloot hij zijn vaardigheden in te zetten om desinformatie te bestrijden.
