import { Languages } from "@trust-cafe/i18n";
import { ProcessedTOCNode } from "./utils/toc";
export { Languages };

export interface PageInfo {
    name: string,
    filename: string,
    lang: Languages,
    title: string
}

export interface PageData {
    lang: Languages,
    toc: ProcessedTOCNode | undefined,
    md: string,
    html: string,
    title: string,
    filename: string;
}

export interface ContentResult {
    data: PageData & PageInfo,
    error: string[];
}

export interface DefaultOptions {
    defaultLanguage: Languages;
}
