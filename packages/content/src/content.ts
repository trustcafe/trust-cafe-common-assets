
import { Languages, ContentResult, DefaultOptions, PageData, PageInfo } from "./types";
import { pages } from "./_generated"


const fixLangPath = (lang: string) => {
    switch(lang){
        case 'en':
            return 'en-gb';
        default:
            return lang;
    }
}


/*
 * Fetches the content file for a given language and page.
 * @param {Languages} lang - The language code.
 * @param {string} page - The page identifier.
 * @returns {Promise<ContentResult>} The content result containing data and errors.
 */
export const getContentFile = async (lang: Languages, page: string): Promise<ContentResult> => {

    const pageInfo = pages.find(p => p.lang === fixLangPath(lang) && p.name === page);

    try {
        if (!pageInfo) {
            throw new Error('Content not found');
        }

        const modulePath = `./_generated/${fixLangPath(lang)}/index`;
        const contentModule = await import(modulePath);

        const pageData = contentModule.default[page];

        // console.log(pageData);

        return {
            data: {...pageInfo, ...pageData},
            error: []
        };
    } catch (error) {
        type combinedType = PageInfo & PageData;
        const data: combinedType = {
            name: pageInfo?.name || '',
            filename: pageInfo?.filename || '',
            lang: pageInfo?.lang || '',
            md: '',
            html: '',
            title: '',
            toc: undefined
        }
        return {
            data,
            error: [error instanceof Error ? error.message : 'Unknown error', `Content for ${page} not found in language ${lang}`]
        };
    }
};

export const defaultOptions: DefaultOptions = {
    defaultLanguage: Languages.EN
};

/**
 * Fetches the content for a given language and page, falling back to the default language if necessary.
 * @param {Languages} lang - The language code.
 * @param {string} page - The page identifier.
 * @param {DefaultOptions} [opts] - Optional default options.
 * @returns {Promise<ContentResult>} The content result containing data and errors.
 */
export const getContent = async (lang: Languages, page: string, opts?: DefaultOptions): Promise<ContentResult> => {
    const options = { ...defaultOptions, ...opts };

    const data = await getContentFile(lang, page);
    if (data.error.length > 0) {
        return await getContentFile(options.defaultLanguage, page);
    }
    return data;
};

export const getAllPages = ()=>{
    return pages
}