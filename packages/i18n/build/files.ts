import * as fs from 'fs';
import * as path from 'path';
import { upperSnakeCase } from '@sil/case';

// Directory containing the .json files
const jsonDir = path.join(__dirname, '../src/data');
const outputDir = path.join(__dirname, '../src/_generated');

// Ensure the output directory exists
if (!fs.existsSync(outputDir)) {
  fs.mkdirSync(outputDir, { recursive: true });
}

// Read all .json files in the directory
const jsonFiles = fs.readdirSync(jsonDir).filter(file => file.endsWith('.json'));

// Compile each .json file into a .js file
jsonFiles.forEach(file => {
  const filePath = path.join(jsonDir, file);
  const jsonData = fs.readFileSync(filePath, 'utf-8');
  const moduleName = upperSnakeCase(file.replace('.json', ''));
  const jsContent = `export const DATA_${moduleName} = ${jsonData};
    export default DATA_${moduleName};
  `;
  const outputFilePath = path.join(outputDir, file.replace('.json', '.ts'));
  fs.writeFileSync(outputFilePath, jsContent, 'utf-8');
});

// Generate index.ts file
const indexPath = path.join(outputDir, 'index.ts');

const importStatements = jsonFiles.map(file => {
  const moduleName = upperSnakeCase(file.replace('.json', ''));
  const filePath = `./${file.replace('.json', '')}`;
  return `import DATA_${moduleName} from '${filePath}';`;
});

const exportStatements = `export {\n  ${jsonFiles.map(file => {
  const moduleName = `DATA_` + upperSnakeCase(file.replace('.json', ''));
  return `${moduleName}`;
}).join(',\n  ')}\n};`;

const indexContent = `${importStatements.join('\n')}\n\n${exportStatements}\n`;
fs.writeFileSync(indexPath, indexContent, 'utf-8');

console.log('JSON files compiled to JS and index.ts generated.');
