export const Languages = {
    EN_GB: 'en-gb',
    EN_US: 'en-us',
    RU: 'ru',
    DE_DE: 'de-de',
    ES_ES: 'es-es',
    PT_BR: 'pt-br',
    NL_NL: 'nl-nl',
    // Fallbacks;

    EN: 'en-gb',
    DE: 'de-de',
    ES: 'es-es',
    PT: 'pt-br',
    NL: 'nl-nl'
}

export * from "./_generated";

export type Languages = (typeof Languages)[keyof typeof Languages];
export const getLanguage = async (lang: Languages): Promise<Object> => {

    // Create an async import function to load the .json files.
    const importLanguage = async (lang: Languages) => {
        switch (lang) {
            case Languages.RU:
                return await import('./data/ru.json');
            case Languages.DE:
            case Languages.DE_DE:
                return await import('./data/de-de.json');
            case Languages.ES:
            case Languages.ES_ES:
                return await import('./data/es-es.json');
            case Languages.PT:
            case Languages.PT_BR:
                return await import('./data/pt-br.json');
            case Languages.NL:
            case Languages.NL_NL:
                return await import('./data/nl-nl.json');
            case Languages.EN:
            case Languages.EN_GB:
            case Languages.EN_US:
            default:
                return await import('./data/en-gb.json');
        }
    }

    try {
        const language = await importLanguage(lang);
        return language;
    } catch (e) {
        return {};
    }
}